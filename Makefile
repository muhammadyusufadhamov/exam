DB_URL=postgresql://postgres:postgres@localhost:5432/excel_exam?sslmode=disable

migrateup:
	migrate -path migration -database "$(DB_URL)" -verbose up

migratedown:
	migrate -path migration -database "$(DB_URL)" -verbose down

swag-init:
	swag init -g api/api.go -o api/docs