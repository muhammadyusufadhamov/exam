FROM golang:1.19.1-alpine3.16 as builder

WORKDIR /excel

COPY . .

RUN apk add curl
RUN go build -o main cmd/main.go
RUN curl -L https://github.com/golang-migrate/migrate/releases/download/v4.15.2/migrate.linux-amd64.tar.gz | tar xvz

FROM alpine:3.16

WORKDIR /excel
RUN mkdir excel_file

COPY --from=builder /excel/main .
COPY --from=builder /excel/migrate ./migrate
COPY migration ./migration

EXPOSE 8000

CMD [ "/excel/main" ]