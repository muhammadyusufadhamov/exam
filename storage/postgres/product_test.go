package postgres_test

import (
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/muhammadyusufadhamov/exam/storage/repo"
)

func TestInsertProducts(t *testing.T) {
	arg := repo.Products{
		Products: []*repo.Product{
			{
				Name: "Iphone 14",
				Sku: "DJ45678GA",
				Description: "it is comfortable smartphone",
				Price: 1500,
				Count: 1,
			},
		},
	}
	err := strg.Product().InputData(&arg)
	require.NoError(t, err)
}

func TestGetAll(t *testing.T) {
	products, err := strg.Product().GetAll(&repo.GetAllProductsParams{
		Limit: 10,
		Page:  1,
	})
	require.NoError(t, err)

	require.NotEmpty(t, products)
	require.Equal(t, 1, len(products.Products))
}