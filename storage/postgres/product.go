package postgres

import (
	"database/sql"
	"errors"
	"fmt"

	"github.com/jmoiron/sqlx"
	"gitlab.com/muhammadyusufadhamov/exam/storage/repo"
)

type productRepo struct {
	db *sqlx.DB
}

func NewProduct(db *sqlx.DB) repo.ProductStorageI {
	return &productRepo {
		db: db,
	}
}

func (p *productRepo) GetAll(params *repo.GetAllProductsParams) (*repo.Products, error) {
	result := repo.Products{
		Products: make([]*repo.Product, 0),
	}
	
	offset := (params.Page - 1) * params.Limit

	limit := fmt.Sprintf(" limit %d offset %d ", params.Limit, offset)

	filter := ""
	if params.Search != "" {
		str := "%" + params.Search + "%"
		filter += fmt.Sprintf(
			` where name ilike '%s' or sku ilike '%s' or description ilike '%s' `,
			str, str, str,
		)
	}

	query := `select 
				id,
				name,
				sku,
				description,
				price,
				count,
				created_at
		from products
		` + filter + `
		order by created_at desc
		` + limit

	rows, err := p.db.Query(query)
	if err != nil {
		return nil, err
	}

	for rows.Next() {
		var product repo.Product

		err := rows.Scan(
			&product.ID,
			&product.Name,
			&product.Sku,
			&product.Description,
			&product.Price,
			&product.Count,
			&product.CreatedAt,
		)
		if err != nil {
			return nil, err
		}
		result.Products = append(result.Products, &product)
	}
	queryCount := "select count(1) from products " + filter
	if err = p.db.QueryRow(queryCount).Scan(&result.Count); err != nil {
		return nil, err
	}
	return &result, nil
}

func (p *productRepo) GetSku(sku string) (int64, error) {
	query := `
		select id from products where sku = $1
	`

	var id int64
	err := p.db.QueryRow(
		query,
		sku,
	).Scan(&id)
	if err != nil {
		return 0, err
	}
	return id, nil
}

func (p *productRepo) InputData(product *repo.Products) error {
	insertQuery := `
		insert into products (
			name,
			sku,
			description,
			price,
			count
		) values ($1, $2, $3, $4, $5)
	`

	updateQuery := `
		update products set
			name = $1,
			description = $2,
			price = $3,
			count = count + $4
		where id = $5
	`

	for _, product := range product.Products {
		id, err := p.GetSku(product.Sku)
		if err != nil {
			if errors.Is(err, sql.ErrNoRows) {
				r, err := p.db.Exec(
					insertQuery,
					product.Name,
					product.Sku,
					product.Description,
					product.Price,
					product.Count,
				)
				if res, _ := r.RowsAffected(); res == 0 {
					return err
				}	
				continue
			}
			return err
		}
		r, err := p.db.Exec(
			updateQuery,
			product.Name,
			product.Description,
			product.Price,
			product.Count,
			id,
		)
		if res, _ := r.RowsAffected(); res == 0 {
			return err
		}
	}
	return nil 
}