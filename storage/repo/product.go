package repo

import "time"

type Product struct {
	ID int64
	Name string
	Sku string
	Description string
	Price float64
	Count int64
	CreatedAt time.Time
}

type Products struct {
	Products []*Product
	Count int64
}

type GetAllProductsParams struct {
	Limit int64
	Page int64
	Search string
}

type ProductStorageI interface {
	GetAll(param *GetAllProductsParams) (*Products, error)
	InputData(product *Products) error
}
