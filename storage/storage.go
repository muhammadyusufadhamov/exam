package storage

import (

	"github.com/jmoiron/sqlx"
	"gitlab.com/muhammadyusufadhamov/exam/storage/postgres"
	"gitlab.com/muhammadyusufadhamov/exam/storage/repo"
)

type StorageI interface {
	Product() repo.ProductStorageI
}

type storagePG struct {
	productRepo repo.ProductStorageI
}

func NewStorageDB(db *sqlx.DB) StorageI {
	return &storagePG{
		productRepo: postgres.NewProduct(db),
	}
}

func (s *storagePG) Product()repo.ProductStorageI {
	return s.productRepo
}
