create table if not exists products (
    id serial primary key,
    name varchar,
    sku varchar,
    description varchar,
    price decimal(18, 2),
    count int default 1,
    created_at timestamp default current_timestamp
);