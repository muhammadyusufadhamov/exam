package main

import (
	"fmt"
	"log"

	"gitlab.com/muhammadyusufadhamov/exam/config"
	"gitlab.com/muhammadyusufadhamov/exam/storage"
	"gitlab.com/muhammadyusufadhamov/exam/api"

	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
)

func main() {
	cfg := config.Load(".")

	psqlUrl := fmt.Sprintf("host=%s port=%s user=%s password=%s dbname=%s sslmode=disable",
		cfg.Postgres.Host,
		cfg.Postgres.Port,
		cfg.Postgres.User,
		cfg.Postgres.Password,
		cfg.Postgres.Database,
	)

	psqlConn, err := sqlx.Connect("postgres", psqlUrl)
	if err != nil {
		log.Fatalf("failed to connect database: %v", err)
	}

	strg := storage.NewStorageDB(psqlConn)

	apiServer := api.New(&api.RouterOptions{
		Cfg:      &cfg,
		Storage:  strg,
	})

	err = apiServer.Run(cfg.HttpPort)
	if err != nil {
		log.Fatalf("failed to run server: %v", err)
	}

	log.Print("Server stopped")
}
