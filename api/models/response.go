package models

type ErrorResponse struct {
	Error string `json:"error"`
}

type ResponseOk struct {
	Message string `json:"message"`
}