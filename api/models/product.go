package models

import "time"

type Product struct {
	ID int64 `json:"id"`
	Name string `json:"name"`
	Sku string 	`json:"sku"`
	Description string `json:"description"`
	Price float64 `json:"price"`
	Count int64 `json:"count"`
	CreatedAt time.Time `json:"created_at"`
}

type GetAllProductsParams struct {
	Limit int64 `json:"limit" binding:"required" default:"10"`
	Page int64 `json:"page" binding:"required" default:"1"`
	Search string `json:"search"`
}

type GetAllProductsResponse struct {
	Products []*Product `json:"products"`
	Count int64 `json:"count"`
}