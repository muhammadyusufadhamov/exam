package v1

import (
	"errors"
	"mime/multipart"
	"net/http"
	"os"
	"path/filepath"
	"strconv"

	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"github.com/xuri/excelize/v2"
	"gitlab.com/muhammadyusufadhamov/exam/api/models"
	"gitlab.com/muhammadyusufadhamov/exam/storage/repo"
)

type File struct {
	File *multipart.FileHeader `form:"file" binding:"required"`
}

// @Router /products [get]
// @Summary Get products
// @Description Get products
// @Tags file-upload
// @Accept json
// @Produce json
// @Param params query models.GetAllProductsParams false "Params"
// @Success 200 {object} models.GetAllProductsResponse
// @Failure 500 {object} models.ErrorResponse
func (h *handlerV1) GetAllProduct(ctx *gin.Context) {
	params, err := validateGetAllParams(ctx)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}

	result, err := h.storage.Product().GetAll(&repo.GetAllProductsParams{
		Page:   params.Page,
		Limit:  params.Limit,
		Search: params.Search,
	})
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}

	ctx.JSON(http.StatusOK, getProductsResponse(result))
}

func getProductsResponse(data *repo.Products) *models.GetAllProductsResponse {
	response := models.GetAllProductsResponse{
		Products: make([]*models.Product, 0),
		Count:    data.Count,
	}

	for _, product := range data.Products {
		u := parseProductModel(product)
		response.Products = append(response.Products, &u)
	}

	return &response
}

func parseProductModel(product *repo.Product) models.Product {
	return models.Product{
		ID:          product.ID,
		Name:        product.Name,
		Sku:         product.Sku,
		Description: product.Description,
		Price:       product.Price,
		Count:       product.Count,
		CreatedAt:   product.CreatedAt,
	}
}

// @Router /file-upload [post]
// @Summary File upload
// @Description File upload
// @Tags file-upload
// @Accept json
// @Produce json
// @Param file formData file true "File"
// @Success 200 {object} models.ResponseOk
// @Failure 500 {object} models.ErrorResponse
func (h *handlerV1) FileUpload(ctx *gin.Context) {
	var file File

	err := ctx.ShouldBind(&file)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, errorResponse(err))
		return
	}

	id := uuid.New()
	path := filepath.Ext(file.File.Filename)
	if !checkPath(path) {
		ctx.JSON(http.StatusBadRequest, errorResponse(errors.New("not allowed excel file")))
		return
	}
	fileName := id.String() + filepath.Ext(file.File.Filename)
	dst, _ := os.Getwd()

	if _, err := os.Stat(dst + "/excel_file"); os.IsNotExist(err) {
		os.Mkdir(dst+"/excel_file", os.ModePerm)
	}

	filePath := "/excel_file/" + fileName
	err = ctx.SaveUploadedFile(file.File, dst+filePath)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}
	f, err := excelize.OpenFile(dst + filePath)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}

	defer func() {
		if err := f.Close(); err != nil {
			ctx.JSON(http.StatusInternalServerError, errorResponse(err))
			return
		}
	}()

	rows, err := f.GetRows("Sheet1")
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}
	if len(rows) == 0 {
		ctx.JSON(http.StatusBadRequest, errorResponse(errors.New("empty file can not add to database")))
	}

	products := repo.Products{
		Products: make([]*repo.Product, 0),
	}

	for _, row := range rows {
		var p repo.Product
		p.Name = row[0]
		p.Sku = row[1]
		p.Description = row[2]
		p.Price, err = strconv.ParseFloat(row[3], 64)
		if err != nil {
			ctx.JSON(http.StatusInternalServerError, errorResponse(err))
			return
		}
		p.Count, err = strconv.ParseInt(row[4], 10, 64)
		if err != nil {
			ctx.JSON(http.StatusInternalServerError, errorResponse(err))
			return
		}
		products.Products = append(products.Products, &p)
	}

	err = h.storage.Product().InputData(&products)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, errorResponse(err))
		return
	}

	ctx.JSON(http.StatusOK, models.ResponseOk{
		Message: "Succesfully uploaded and added to database",
	})
}
