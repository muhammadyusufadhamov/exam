package v1

import (
	"strconv"

	"github.com/gin-gonic/gin"
	"gitlab.com/muhammadyusufadhamov/exam/api/models"
	"gitlab.com/muhammadyusufadhamov/exam/config"
	"gitlab.com/muhammadyusufadhamov/exam/storage"
)

type handlerV1 struct {
	cfg      *config.Config
	storage  storage.StorageI
}

type HabdlerV1Options struct {
	Cfg      *config.Config
	Storage  storage.StorageI
}

func New(options *HabdlerV1Options) *handlerV1 {
	return &handlerV1{
		cfg:      options.Cfg,
		storage:  options.Storage,
	}
}

func errorResponse(err error) *models.ErrorResponse {
	return &models.ErrorResponse{
		Error: err.Error(),
	}
}

func checkPath(path string) bool {
	if path != ".xlsx" && path != ".xlsm" && path != ".xlsb" && path != ".xltx" && path != ".xls" && path != ".xlt" && path != ".xml" && path != ".xlam" && path != ".xla" && path != ".xlw" && path != ".xlr" {
		return false
	}
	return true
}

func validateGetAllParams(c *gin.Context) (*models.GetAllProductsParams, error) {
	var (
		limit int64 = 10
		page  int64 = 1
		err   error
	)

	if c.Query("limit") != "" {
		limit, err = strconv.ParseInt(c.Query("limit"), 10, 64)
		if err != nil {
			return nil, err
		}
	}

	if c.Query("page") != "" {
		page, err = strconv.ParseInt(c.Query("page"), 10, 64)
		if err != nil {
			return nil, err
		}
	}
	return &models.GetAllProductsParams{
		Limit:  limit,
		Page:   page,
		Search: c.Query("search"),
	}, nil
}