package api

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/muhammadyusufadhamov/exam/config"
	"gitlab.com/muhammadyusufadhamov/exam/storage"
	"gitlab.com/muhammadyusufadhamov/exam/api/v1"

	swaggerFiles "github.com/swaggo/files"     // swagger embed files
	ginSwagger "github.com/swaggo/gin-swagger" // gin-swagger middleware

	_ "gitlab.com/muhammadyusufadhamov/exam/api/docs"
)

type RouterOptions struct {
	Cfg      *config.Config
	Storage  storage.StorageI
}

// @title           Swagger for blog api
// @version         1.0
// @description     This is a excel file upload api.
// @BasePath  /v1
func New(opt *RouterOptions) *gin.Engine {
	router := gin.Default()

	handlerV1 := v1.New(&v1.HabdlerV1Options{
		Cfg:      opt.Cfg,
		Storage:  opt.Storage,
	})

	apiV1 := router.Group("/v1")

	apiV1.GET("/products", handlerV1.GetAllProduct)
	apiV1.POST("/file-upload", handlerV1.FileUpload)

	router.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))

	return router
}